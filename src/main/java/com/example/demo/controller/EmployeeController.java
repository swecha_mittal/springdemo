package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Validation;
import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;

@RestController
@RequestMapping("/demo")
public class EmployeeController {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private Validation validation;

	@PostMapping("/add")
	/**
	 * POST API to add an employee
	 * 
	 * @param employee request containing employee data
	 * @return response containing added employee if successful otherwise response
	 *         containing error
	 */
	public @ResponseBody Employee addEmployee(@RequestBody Employee employee) {
		Employee newEmployee = new Employee();

		// method calling for checking the Employee name is empty and responding with
		// error
		if (validation.isEmployeeNameEmpty(employee)) {
			newEmployee.setError("Employee name missing");
			return newEmployee;
		}

		// method calling for checking the Employee phone is empty and responding with
		// error
		if (validation.isEmployeePhoneEmpty(employee)) {
			newEmployee.setError("Employee's phone number missing");
			return newEmployee;
		}

		// method calling for checking the Employee email is empty and responding with
		// error
		if (validation.isEmployeeEmailEmpty(employee)) {
			newEmployee.setError("Employee's email missing");
			return newEmployee;
		}

		// method calling for checking the Employee name is empty and responding with
		// error
		if (validation.isEmployeeDobEmpty(employee)) {
			newEmployee.setError("DOB missing");
			return newEmployee;
		}

		// method calling for checking the Employee email is unique and responding with
		// error
		if (validation.isEmployeeEmailNotUnique(employee)) {
			newEmployee.setError("Enter unique Email");
			return newEmployee;
		}

		newEmployee.setemployee_name(employee.getemployee_name());
		newEmployee.setEmail(employee.getEmail());
		newEmployee.setemployee_phone(employee.getemployee_phone());
		newEmployee.setDob(employee.getDob());
		newEmployee = employeeRepository.save(newEmployee);
		newEmployee.setDob(null);
		newEmployee.setEmail(null);
		newEmployee.setemployee_name(null);
		newEmployee.setemployee_phone(null);
		newEmployee.setSuccess("Employee added successfully");
		return newEmployee;
	}

	@GetMapping("/all")
	/**
	 * GET API to display all the employees
	 * 
	 * @return response containing all employees information
	 */
	public @ResponseBody Iterable<Employee> getAllUsers() {
		return employeeRepository.findAll();
	}

	@GetMapping("/employee/{employeeId}")
	/**
	 * GET API to display an employee
	 * 
	 * @param employeeId variable containing employee Id
	 * @return response containing employee information if successful otherwise
	 *         error
	 */
	public Employee findOne(@PathVariable Integer employeeId) {
		Employee employee = new Employee();

		// method calling for checking whether Employee ID exist and responding with
		// error
		if (validation.doesnotExistById(employeeId, employee)) {
			employee.setError("Employee doesnot exist");
			return employee;
		}

		employee = employeeRepository.findById(employeeId).get();
		return employee;
	}

	@PutMapping("/edit/{employeeId}")
	/**
	 * PUT API to update an employee
	 * 
	 * @param employeeId variable containing employee Id
	 * @param employee   request containing employee data
	 * @return response containing updated employee information if successful
	 *         otherwise error
	 */
	public Employee updateEmployee(@PathVariable("employeeId") Integer employeeId, @RequestBody Employee employee) {
		Employee existingEmployee = new Employee();

		// method calling for checking whether Employee ID exist and responding with
		// error
		if (validation.doesnotExistById(employeeId, existingEmployee)) {
			existingEmployee.setError("Employee doesnot exist");
			return existingEmployee;
		}

		existingEmployee = employeeRepository.findById(employeeId).get();

		// checking if the name field is not null and updating the following
		if (employee.getemployee_name() != null) {
			existingEmployee.setemployee_name(employee.getemployee_name());
		}

		// checking if the phone field is not null and updating the following
		if (employee.getemployee_phone() != null) {
			existingEmployee.setemployee_phone(employee.getemployee_phone());
		}

		// checking if the DOB field is not null and updating the following
		if (employee.getDob() != null) {
			existingEmployee.setDob(employee.getDob());
		}
		employeeRepository.save(existingEmployee);
		existingEmployee.setSuccess("Employee Successfully updated");
		return existingEmployee;

	}

	@DeleteMapping("/deleteEmployee/{employeeId}")
	/**
	 * DELETE API to delete an employee
	 * 
	 * @param employeeId variable containing employee Id
	 * @return response containing deleted employee if successful otherwise error
	 */
	public Employee deleteEmployee(@PathVariable("employeeId") Integer employeeId) {
		Employee existingEmployee = new Employee();

		// method calling for checking whether Employee ID exist and responding with
		// error
		if (validation.doesnotExistById(employeeId, existingEmployee)) {
			existingEmployee.setError("Employee doesnot exist");
			return existingEmployee;
		}

		employeeRepository.deleteById(employeeId);
		existingEmployee.setSuccess("Employee deleted successfully");
		return existingEmployee;
	}
}
