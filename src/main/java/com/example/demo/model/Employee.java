package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

//import com.fasterxml.jackson.databind.JsonSerializable;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "employee_id")
	private Integer employee_id;

	@Column(unique = true, name = "employee_phone")
	private Integer employee_phone;

	@Column(nullable = false, name = "employee_name")
	private String employee_name;

	@Column(unique = true, name = "email")
	private String email;

	@Column(name = "dob")
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date dob;

	private String error;
	private String success;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Integer getemployee_id() {
		return this.employee_id;
	}

	public void setemployee_id(Integer employeeId) {
		this.employee_id = employeeId;
	}

	public Integer getemployee_phone() {
		return this.employee_phone;
	}

	public void setemployee_phone(Integer employee_phone) {
		this.employee_phone = employee_phone;
	}

	public String getemployee_name() {
		return this.employee_name;
	}

	public void setemployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

}