package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.PathVariable;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;

@Component
public class Validation {

	@Autowired
	private EmployeeRepository employeeRepository;

	/**
	 * 
	 * @param employeeId variable containing employee Id
	 * @param employee   request containing employee data
	 * @return response containing boolean value true if employee id exists
	 *         otherwise false
	 */
	public Boolean doesnotExistById(Integer employeeId, Employee employee) {
		if (!employeeRepository.existsById(employeeId)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param employee    request containing employee data
	 * @param newEmployee parameters to add information of new employee
	 * @return response containing boolean value true if employee name is empty
	 *         otherwise false
	 */
	public Boolean isEmployeeNameEmpty(Employee employee) {
		if (employee.getemployee_name() == null || employee.getemployee_name().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param employee    request containing employee data
	 * @param newEmployee parameters to add information of new employee
	 * @return response containing boolean value true if employee phone is empty
	 *         otherwise false
	 */
	public Boolean isEmployeePhoneEmpty(Employee employee) {
		if (employee.getemployee_phone() == null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param employee    request containing employee data
	 * @param newEmployee parameters to add information of new employee
	 * @return response containing boolean value true if employee email is empty
	 *         otherwise false
	 */
	public Boolean isEmployeeEmailEmpty(Employee employee) {
		if (employee.getEmail() == null || employee.getEmail().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param employee    request containing employee data
	 * @param newEmployee parameters to add information of new employee
	 * @return response containing boolean value true if employee DOB is empty
	 *         otherwise false
	 */
	public Boolean isEmployeeDobEmpty(Employee employee) {
		if (employee.getDob() == null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param employee    request containing employee data
	 * @param newEmployee parameters to add information of new employee
	 * @return response containing boolean value true if employee email is not
	 *         unique otherwise false
	 */
	public Boolean isEmployeeEmailNotUnique(Employee employee) {
		if (employeeRepository.findByEmail(employee.getEmail()) != null) {
			return true;
		}
		return false;
	}
}
