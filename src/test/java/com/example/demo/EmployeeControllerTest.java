package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.controller.EmployeeController;
import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeControllerTest {

	@InjectMocks
	EmployeeController employeeController;

	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	Validation validation;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
//
//		Employee employee1=new Employee();
//		employee1.setemployee_name("Max");
//		employee1.setemployee_phone(244243);
//		employee1.setemployee_id(1);
//		employee1.setEmail("Max@email.au");
//		employee1.setDob(new Date(1999-10-10));

	@Test
	public void checks_getting_all_users() {
		Employee employee1 = new Employee();
		employee1.setemployee_name("Max");
		employee1.setemployee_phone(244243);
		employee1.setemployee_id(1);
		employee1.setEmail("Max@email.au");
		employee1.setDob(new Date(1999 - 10 - 10));

		Employee employee2 = new Employee();
		employee2.setemployee_name("Sam");
		employee2.setemployee_phone(243343);
		employee2.setemployee_id(3);
		employee2.setEmail("sam@email.au");
		employee2.setDob(new Date(1999 - 10 - 10));

		ArrayList<Employee> employees = new ArrayList<Employee>();
		employees.add(employee1);
		employees.add(employee2);

		Mockito.when(employeeRepository.findAll()).thenReturn(employees);

		Iterable<Employee> result = employeeController.getAllUsers();
		assertThat(((ArrayList<Employee>) result).size()).isEqualTo(2);
		assertThat(((ArrayList<Employee>) result).get(0).getemployee_name()).isEqualTo(employee1.getemployee_name());
		assertThat(((ArrayList<Employee>) result).get(1).getemployee_name()).isEqualTo(employee2.getemployee_name());
	}

	@Test
	public void checks_getting_by_id() {
		Employee employee1 = new Employee();
		employee1.setemployee_name("Max");
		employee1.setemployee_phone(244243);
		employee1.setemployee_id(1);
		employee1.setEmail("Max@email.au");
		employee1.setDob(new Date(1999 - 10 - 10));
//		Mockito.when(validation.doesnotExistById(1,employee1)).thenReturn(false);
		Optional<Employee> opt = Optional.of(employee1);
		Mockito.when(employeeRepository.findById(1)).thenReturn(opt);
//		Mockito.when(employeeRepository.findById(1).get()).thenReturn(employee1);
		Mockito.when(employeeRepository.existsById(1)).thenReturn(true);
		Employee result = employeeController.findOne(1);
		assertEquals("Max", result.getemployee_name());
		assertEquals(new Integer(244243), result.getemployee_phone());
		assertEquals("Max@email.au", result.getEmail());
		assertEquals(new Date(1999 - 10 - 10), result.getDob());

	}

	@Test
	public void checks_adding_employee() {
		Employee employee1 = new Employee();
		employee1.setemployee_name("Max");
		employee1.setemployee_phone(244243);
		employee1.setemployee_id(1);
		employee1.setEmail("Max@email.au");
		employee1.setDob(new Date(1999 - 10 - 10));
		Mockito.when(employeeRepository.save(Mockito.any())).thenReturn(employee1);
		Employee result = employeeController.addEmployee(employee1);

		assertEquals(new Integer(1), result.getemployee_id());
		assertEquals("Employee added successfully", result.getSuccess());
//		assertEquals(null, result.getemployee_name());

	}

	@Test
	public void checks_deleting_employee() {
		Employee employee1 = new Employee();
		employee1.setemployee_name("Max");
		employee1.setemployee_phone(244243);
		employee1.setemployee_id(1);
		employee1.setEmail("Max@email.au");
		employee1.setDob(new Date(1999 - 10 - 10));
//		Mockito.when(employeeRepository.deleteById(1)).thenReturn(null);
		Employee result = employeeController.deleteEmployee(1);
		assertEquals("Employee deleted successfully", result.getSuccess());

	}

	@Test
	public void checks_updating_employee() {
		Employee employee1 = new Employee();
		;
		employee1.setemployee_name("Max");
		employee1.setemployee_phone(244243);
		employee1.setemployee_id(1);
		employee1.setEmail("Max@email.au");
		employee1.setDob(new Date(1999 - 10 - 10));
		Optional<Employee> opt = Optional.of(employee1);
		Mockito.when(employeeRepository.findById(1)).thenReturn(opt);
		Employee result = employeeController.updateEmployee(1, employee1);
		assertEquals("Employee Successfully updated", result.getSuccess());
	}

	@Test
	public void checks_empty_name_adding_employee() {
		Employee employee = new Employee();
		Mockito.when(validation.isEmployeeNameEmpty(employee)).thenReturn(true);
		Employee result = employeeController.addEmployee(employee);
		assertEquals("Employee name missing", result.getError());

	}

	@Test
	public void checks_empty_phone_adding_employee() {
		Employee employee = new Employee();
		Mockito.when(validation.isEmployeePhoneEmpty(employee)).thenReturn(true);
		Employee result = employeeController.addEmployee(employee);
		assertEquals("Employee's phone number missing", result.getError());

	}

	@Test
	public void checks_empty_email_adding_employee() {
		Employee employee = new Employee();
		Mockito.when(validation.isEmployeeEmailEmpty(employee)).thenReturn(true);
		Employee result = employeeController.addEmployee(employee);
		assertEquals("Employee's email missing", result.getError());

	}

	@Test
	public void checks_empty_dob_adding_employee() {
		Employee employee = new Employee();
		Mockito.when(validation.isEmployeeDobEmpty(employee)).thenReturn(true);
		Employee result = employeeController.addEmployee(employee);
		assertEquals("DOB missing", result.getError());

	}

	@Test
	public void checks_unique_email_adding_employee() {
		Employee employee = new Employee();
		Mockito.when(validation.isEmployeeEmailNotUnique(employee)).thenReturn(true);
		Employee result = employeeController.addEmployee(employee);
		assertEquals("Enter unique Email", result.getError());

	}

	@Test
	public void checks_employeeid_doesnot_exists_updateEmployee() {
		Employee employee = new Employee();
		Mockito.when(validation.doesnotExistById(Mockito.any(), Mockito.any())).thenReturn(true);
		Employee result = employeeController.updateEmployee(1, employee);
		assertEquals("Employee doesnot exist", result.getError());
	}

	@Test
	public void checks_employeeid_doesnot_exists_deleteEmployee() {
		Mockito.when(validation.doesnotExistById(Mockito.any(), Mockito.any())).thenReturn(true);
		Employee result = employeeController.deleteEmployee(1);
		assertEquals("Employee doesnot exist", result.getError());
	}

	@Test
	public void checks_employeeid_doesnot_exists_findOne() {
		Employee employee = new Employee();
		Mockito.when(validation.doesnotExistById(Mockito.any(), Mockito.any())).thenReturn(true);
		Employee result = employeeController.findOne(1);
		assertEquals("Employee doesnot exist", result.getError());
	}
}
