package com.example.demo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Date;

import org.junit.Before;
//import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidationTest {

	Employee employee = new Employee();

	@InjectMocks
	@Autowired
	private Validation validation;

	@Mock
	EmployeeRepository employeeRepository;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void checks_employeeid_exists() {
		Mockito.when(employeeRepository.existsById(1)).thenReturn(true);
		Boolean result = validation.doesnotExistById(1, employee);
		assertFalse(result);
	}

	@Test
	public void checks_employeeid_does_not_exist() {
		Mockito.when(employeeRepository.existsById(1)).thenReturn(false);
		Boolean result = validation.doesnotExistById(1, employee);
		assertTrue(result);
	}

	@Test
	public void checks_employeename_is_null() {
		Employee newEmployee = new Employee();
		employee.setemployee_name(null);
		Boolean result = validation.isEmployeeNameEmpty(employee);
		assertTrue(result);
	}

	@Test
	public void checks_employee_name_is_empty() {
		Employee newEmployee = new Employee();
		employee.setemployee_name("");
		Boolean result = validation.isEmployeeNameEmpty(employee);
		assertTrue(result);
	}

	@Test
	public void checks_employee_name_is_non_empty_non_null() {
		Employee newEmployee = new Employee();
		employee.setemployee_name("Yash");
		Boolean result = validation.isEmployeeNameEmpty(employee);
		assertFalse(result);
	}

	@Test
	public void checks_employee_phone_is_null() {
		Employee newEmployee = new Employee();
		employee.setemployee_phone(null);
		Boolean result = validation.isEmployeePhoneEmpty(employee);
		assertTrue(result);
	}

	@Test
	public void checks_employee_phone_is_not_null() {
		Employee newEmployee = new Employee();
		employee.setemployee_phone(4353452);
		Boolean result = validation.isEmployeePhoneEmpty(employee);
		assertFalse(result);
	}

	@Test
	public void checks_employee_email_is_null() {
		Employee newEmployee = new Employee();
		employee.setEmail(null);
		Boolean result = validation.isEmployeeEmailEmpty(employee);
		assertTrue(result);
	}

	@Test
	public void checks_employee_email_is_empty() {
		Employee newEmployee = new Employee();
		employee.setEmail("");
		Boolean result = validation.isEmployeeEmailEmpty(employee);
		assertTrue(result);
	}

	@Test
	public void checks_employee_email_is_not_null() {
		Employee newEmployee = new Employee();
		employee.setEmail("yash123@cn.in");
		Boolean result = validation.isEmployeeEmailEmpty(employee);
		assertFalse(result);
	}

	@Test
	public void checks_employee_dob_is_null() {
		Employee newEmployee = new Employee();
		employee.setDob(null);
		Boolean result = validation.isEmployeeDobEmpty(employee);
		assertTrue(result);
	}

	@Test
	public void checks_employee_dob_is_not_null() {
		Employee newEmployee = new Employee();
		employee.setDob(new Date(1997 - 05 - 21));
		Boolean result = validation.isEmployeeDobEmpty(employee);
		assertFalse(result);
	}

	@Test
	public void checks_employeeid_is_unique() {
		Employee newEmployee = new Employee();
		Mockito.when(employeeRepository.findByEmail("Yash123@cn.in")).thenReturn(null);
		employee.setEmail("Yash123@cn.in");
		Boolean result = validation.isEmployeeEmailNotUnique(employee);
		assertFalse(result);
	}

	@Test
	public void checks_employeeid_is_not_unique() {
		Employee newEmployee = new Employee();
		Mockito.when(employeeRepository.findByEmail("Yash123@cn.in")).thenReturn(employee);
		employee.setEmail("Yash123@cn.in");
		Boolean result = validation.isEmployeeEmailNotUnique(employee);
		assertTrue(result);
	}
}
